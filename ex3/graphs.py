#!/bin/python
import matplotlib.pyplot as plt
import numpy as np


graphs = [
    np.loadtxt("1.csv"),
    np.loadtxt("2.csv"),
    np.loadtxt("3.csv"),
    np.loadtxt("4.csv"),
    np.loadtxt("5.csv"),
    np.loadtxt("6.csv"),
    np.loadtxt("7.csv"),
    np.loadtxt("8.csv"),
    np.loadtxt("9.csv"),
    np.loadtxt("10.csv"),
    np.loadtxt("11.csv"),
    np.loadtxt("12.csv"),
]

names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]

def find_point(val, x, y, input_val='x'):
    """Para input_val = 'x', devolve qual o valor no eixo y quando x = val. 'x'
    deve receber o vetor dos valores da funcao no eixo x, e 'y' os valores no
    eixo y.
    Para input_val = 'y', retorna o valor de x em que y = val."""
    if input_val == 'x':
        vec = x
        other_vec = y
    else:
        vec = y
        other_vec = x
    val_vec = [val for _ in range(len(vec))]
    idx = np.argwhere(np.diff(np.sign(vec - val_vec))).flatten()
    output = []
    for i, v in enumerate(idx):
        if i % 2 == 0:
            output.append(other_vec[v])

    return output

# Gera grafico completo, com todas as mudancas de niveis dos comparadores
for graph in graphs:
    plt.plot(graph[:,0], graph[:,1])
plt.legend(names)
plt.xticks(range(0, 11))
plt.grid()
plt.xlabel("Tensão de entrada [V]")
plt.ylabel("Tensão de saída [V]")
plt.savefig("bargraph.png", dpi=300)
plt.clf()

# Gera graficos ampliados, com as curvas dadas pela lista a seguir. Primeiro
# grafico com curvas de 0 a 3, segundo com de 4 a 7 e terceiro com curvas 8 a
# 11.
plots = [(0, 4), (4, 8), (8, 12)]
for plot in plots:
    for graph in graphs[plot[0]:plot[1]]:
        plt.plot(graph[:,0], graph[:,1])
    plt.legend(names[plot[0]:plot[1]])
    plt.xticks(range(0, 11))
    plt.grid()
    plt.xlabel("Tensão de entrada [V]")
    plt.ylabel("Tensão de saída [V]")
    inter_xs = []
    # Encontra transicao da curva (ponto em que passa por 2.5V) e traca uma reta
    # vertical no ponto
    for graph in graphs[plot[0]:plot[1]]:
        inter_x = find_point(2.5, graph[:,0], graph[:,1], 'y')
        inter_xs.append(inter_x)
        plt.axvline(x=inter_x, color='magenta')
    # Garante margem em torno das curvas
    min_x = inter_xs[0][0]
    max_x = inter_xs[-1][0]
    delta_x = max_x - min_x
    margin = 0.3
    min_x -= delta_x * margin
    max_x += delta_x * margin
    plt.xlim((min_x, max_x))
    plt.xticks(inter_xs)
    plt.savefig(f"bargraph_wlines_{plot[0]}_{plot[1]}.png", dpi=300)
    plt.clf()
