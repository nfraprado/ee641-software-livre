#!/bin/python

from numpy import log10

vs = [0.025, 0.125, 0.45, 0.7, 1.05, 1.3, 1.625, 2, 2.45, 3.425, 4.875, 8.975]

output_file = "v_in_db.txt"

def v_to_db(v):
    v0 = 1.6
    return 20 * log10(v / v0)

with open(output_file, 'w') as f:
    for v in vs:
        f.write(f"{v}V = {v_to_db(v)}dB\n")
