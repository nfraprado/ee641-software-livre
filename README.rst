EE641 Software Livre: Resolvendo o laboratório de eletrônica II com software livre
==================================================================================

Motivação
---------

Você é um aluno de engenharia elétrica da Unicamp frustrado com os
laboratórios de eletrônica que sempre exigem o uso do programa de simulação
proprietário pspice?
Você se esforça para utilizar software livre no dia-a-dia, mas o professor
impõe o uso dessa ferramenta, e você se sente pressionado a abrir o Wine no
seu sistema Linux ou mesmo ir para o Windows para poder executar o pspice
Student Edition, afinal, você tem seus princípios, mas precisa passar na
disciplina e se sente sem alternativas.
O professor justifica que essa ferramenta é grátis, mas claro que isso não é o
problema, mas sim o fato de estarem inibindo sua liberdade.
Afinal, você está na universidade para aprender, com qualquer ferramenta de
sua preferência, não para ser preso a uma ferramenta que impõe limitações na
esperança que você por inércia de já saber utilizá-la, compre a versão
completa no futuro quando necessitar de todas as funcionalidades, dando adeus
a sua liberdade.

Bom, independente de quem você seja, era exatamente assim que eu me sentia.
Apesar de alguns professores permitirem a utilização de outro programa,
certamente não incentivavam, e a inércia da parte deles era ainda maior para
prosseguir no uso dessa ferramenta proprietária.
Com base nisso tudo, ao realizar a última disciplina do meu curso, Laboratório
de Eletrônica 2, resolvi que não seria assim de novo.

Em vezes anteriores tentei resistir, mas acabei sendo levado pela inércia.
Mas dessa vez, a quarentena causada pelo corona vírus, que tornava meu
computador pessoal a única opção,  e o semestre leve, por ser meu último, com
apenas 2 disciplinas, me fez decidir fazer diferente.

Vou escolher o caminho mais difícil, não trilhado, de usar ferramentas livres
para completar essa disciplina, e disponibilizar tudo aqui, para que você,
estudante do futuro, não se sinta sob tanta pressão, e possa mais facilmente
optar por não utilizar as ferramentas proprietárias que são impostas.

Como é na maioria das vezes, o mundo de software livre dispõe de diversas
opções, cada uma mais completa no segmento da tarefa que mais interessava a
comunidade que a utiliza, e portanto nenhuma delas tão completa quanto a
ferramenta proprietária que possui mais mão-de-obra por ser direcionada pelo
lucro.

Escolhi utilizar o ngspice para montar e simular os circuitos.
Ele não é tão intuitivo: tudo é realizado por texto, tanto as ligações e
propriedades dos componenetes quanto as simulações que devem ser feitas.
Por outro lado é bastante completo e tem um comunidade dedicada, sendo
inclusive usado por outros projetos.
O mais promissor parece ser o KiCAD, programa para o desenho de esquemático de
circuitos, que possui suporte ao ngspice para realizar a simulação.
No momento achei mais promissor utilizar o ngspice diretamente, apesar de
perder a visualização dos componentes e gráficos, já que o suporte ainda não
me pareceu muito bom.

Para produzir os gráficos a partir da saída da simulação do ngspice, utilizei
a linguagem de programação python e sua biblioteca matplotlib.
O python já se mostrou para mim extremamente flexível e fácil de utilizar, e
sua biblioteca matplotlib produz gráficos de excelente qualidade.

Em suma, esse repositório contém tudo que utilizei para resolver os
experimentos de simulação do laboratório de eletrônica 2 em 2020 utilizando
apenas software livre: ngspice e python.
Espero que seja útil para você e que ajude a proteger sua liberdade.

Estrutura
---------

Cada um dos experimentos é separado por pasta e contém todos os arquivos
necessários para sua replicação:

* Arquivo ``.pdf`` contendo o roteiro de referência para o circuito e simulações
  que devem ser feitas
* Arquivo ``.cir`` contendo as informações para o ngspice: o circuito a ser
  simulado e as análise que devem ser realizadas
* Arquivo ``.py`` contendo o script em python que utiliza os arquivos de saída
  da simulação (``.csv``) para geração dos gráficos
* ``Makefile``, o qual provê os comandos para execução das simulações e geração
  dos gráficos.

Na pasta ``spice_mod`` estão os modelos SPICE dos componentes usados nos
experimentos.

O arquivo ``ngspice.md`` contém um guia para iniciantes sobre o ngspice.

Requisitos
----------

Para que tudo funcione, alguns programas são necessários (todos software livre,
claro).

Para que os comandos de execução da simulação e geração das imagens (contidos no
Makefile) funcionem, é necessário ter o ``make`` instalado.

Para a execução das simulações, é necessário ter o ``ngspice`` instalado.

Para a geração dos gráficos, é necessário ter o ``python`` e suas bibliotecas
``matplotlib`` e ``numpy`` instalados.

Utilização
----------

A utilização é bastante simples: Basta entrar na pasta do experimento/circuito
desejado e digitar o comando ``make``.
Isso realizará as simulações do circuito e gerará os gráficos de saída.

O comando ``make help`` mostra as outras opções disponíveis.

Notas finais
------------

Finalmente, tive o trabalho adicional de documentar tudo que fiz: circuitos,
scripts de geração dos gráficos, Makefile...
Meu objetivo com isso é incentivar que você explore os arquivos e entenda como
tudo está sendo feito.
Além disso, minha esperança é que mesmo caso os experimentos de semestres
futuros sejam diferentes, essas informações lhe ajudem a adaptar tudo que está
aqui para seus circuitos e seus gráficos.
Espero assim que a informação aqui possa ser útil para garantir a autonomia de
estudantes para realizar simulações SPICE quaisquer.
