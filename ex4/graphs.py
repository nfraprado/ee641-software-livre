#!/bin/python
import matplotlib.pyplot as plt
import numpy as np

inp = np.loadtxt("in.csv")

tran = [
    np.loadtxt("out_tran1.csv"),
    np.loadtxt("out_tran2.csv"),
    np.loadtxt("out_tran3.csv"),
    np.loadtxt("out_tran4.csv"),
]

ac = [
    [np.loadtxt("out_ac_mag1.csv"), np.loadtxt("out_ac_mag2.csv"),
     np.loadtxt("out_ac_mag3.csv"), np.loadtxt("out_ac_mag4.csv")],
    [np.loadtxt("out_ac_ph1.csv"), np.loadtxt("out_ac_ph2.csv"),
     np.loadtxt("out_ac_ph3.csv"), np.loadtxt("out_ac_ph4.csv")],
]

names = ["$V_{fet}$ = 0", "$V_{fet}$ = -5", "$V_{fet}$ = -6", "$V_{fet}$ = -10"]

def save_mag_ph_multiplot(magnitudes, phases, names=["out"], filename="plot.png"):
    subplot = [1, 2]
    subplot_y_label = ["Módulo [dB]", "Fase [$\degree$]"]
    plots = [magnitudes, phases]
    for mag_ph in [0, 1]:
        plt.subplot(2, 1, subplot[mag_ph])
        plt.grid(True, which="major", linestyle='-')
        plt.grid(True, which="minor", linestyle='--')

        plt.ylabel(subplot_y_label[mag_ph])
        # So coloca nome no eixo x da fase, porque e o grafico de baixo
        if mag_ph:
            plt.xlabel("Frequência [Hz]")

        for plot in plots[mag_ph]:
            plt.semilogx(plot[:,0], plot[:,1])
        plt.legend(names)

        plt.savefig(filename, dpi=300)
    plt.clf()

plt.plot(inp[:,0], inp[:,1], c='black')
for graph in tran:
    plt.plot(graph[:,0], graph[:,1])
names_tran = ["Entrada"] + names
plt.legend(names_tran)
plt.grid()
plt.xlabel("Tempo [s]")
plt.ylabel("Tensão de saída [V]")
plt.savefig("tran_out.png", dpi=300)
plt.clf()

save_mag_ph_multiplot(ac[0], ac[1], names, "ac_out.png")
