# Comentários sobre o ngspice

O ngspice é um software livre de simulação de circuitos eletrônicos baseado em
SPICE.

Diferentemente dos simuladores que as pessoas estão acostumadas, ele não possui
interface gráfica, e portanto a conexão entre os componentes e a instrução de
quais análises devem ser feitas são escritas em um documento de texto que é lido
pelo programa.
Por mais complicado que pareça, na verdade é bem simples, só é mais difícil de
visualizar o circuito.

Pretendo explicar aqui o básico sobre o ngspice para facilitar sua vida.
Além disso, nos arquivos de circuito .cir de cada um dos experimentos, deixei
comentários explicando o que está sendo feito.
Se isso não bastar, e precisar de mais informações, o manual do programa é a
fonte principal de informação no qual me baseei para quase 100% do que fiz.
O site do programa é http://ngspice.sourceforge.net/ e o manual pode ser obtido
na página [Documentation](http://ngspice.sourceforge.net/docs.html) (e
provavelmente já está no seu computador se já baixou o programa, no meu
computador o manual foi instalado em /usr/share/doc/ngspice/manual.pdf).

Cada componente é indicado por um nome cuja primeira letra indica seu tipo,
seguido de a quais nós cada um de seus terminais está conectado, e finalmente
por seus valores ou parâmetros, que variam de um componente para o outro.

O exemplo mais simples é claro, um resistor:
```
R1 no1 no2 5k
```

Aqui, criei um resistor chamado R1.
O nome de um resistor **precisa** começar com a letra `R`, já que é isso que
indica para o ngspice que ele é um resistor.
`no1` e `no2` são os dois terminais em que o resistor está conectado.
O valor do resistor foi passado como `5k`, ou seja, ele é de 5000 ohms.

**O nó terra é representado pelo nome** `0`.

Para montar o circuito (dois resistores em série, ligando do terra ao terra):
```
      R1       R2
    ------   ------
 ---| 1M |---| 3k |---
 |  ------ ^ ------  |
 =         |         =
GND      meu_no     GND
```

Eu escreveria:
```
R1 0 meu_no 1meg
R2 meu_no 0 3k
```

Onde eu dei o nome de `meu_no` para o nó do meio.
O nome pode ser o que quiser, inclusive pode conter letras, números, símbolos...
com exceção de `,`, `=` e claro, espaço.

Note também que para o resistor de 1 mega ohm, escrevi `1meg`, e não `1M`.
O ngspice é case-insensitive, ou seja, tanto `1m` quanto `1M` significam 1 mili,
e para mega deve ser usado `meg`.

Fora isso, as outras unidades são como esperado: f, p, n, u, m, k, Meg, G, T.
Mesmo para outros componentes, apenas se especifica o valor, e não a unidade,
como ohm, farad, etc.

Um capacitor de 3pF seria:
```
C1 no1 no2 3p
```

Um resistor, capacitor e indutor, são indicados começando com R,
C, e L, respectivamente.

Um BJT é indicado com um `Q`, e seus três nós são, na sequência: coletor, base e
emissor.
Além disso deve ser passado o nome do modelo a ser utilizado (já voltamos
nisso).

Um MOSFET é indicado por `M`, e seus nós são dreno, porta, fonte e substrato.
Também deve ser indicado o modelo a ser usado.

Um modelo spice determina os parâmetros de um componente.
É a parte mais chata de se utilizar SPICE com software livre, já que você
precisa procurar na internet pelo modelo spice do componente que quer simular, e
geralmente eles são protegidos por licença.
Nos programas proprietários muitos modelos spice já vem instalados.

Por exemplo, suponha que eu tenha baixado o seguinte modelo da internet:
```
.model MOD1 npn (bf=50 is=1e-13 vbf=50)
```

Seu nome é MOD1, e define os parâmetros para um transistor BJT NPN.
Para utilizá-lo em um componente basta mencionar seu nome no campo do modelo:
```
Q1 no_c no_b no_e MOD1
```

Assim eu defini um BJT com o coletor conectado em `no_c`, base conectada em
`no_b`, e emissor conectado em `no_e`, e com as características elétricas
definidas por `MOD1`.

Uma fonte de tensão é indicada por um `V`, e pode possuir tanto um valor DC,
quanto um valor AC. 
Uma fonte DC de 5V:
```
Vcc no1 no2 dc 5
```

Uma fonte AC de 127V:
```
Va no1 no2 ac 127
```

Para mais controle podem ser utilizadas funções de forma de onda.
Por exemplo, uma fonte AC senoidal de 60 Hz entre 1 e 4 V (ou seja, 1.5V de
amplitude e 1 V de offset):
```
Vb no1 no2 SIN(1 1.5 60)
```

Uma fonte de corrente funciona da mesma forma, mas é indicada por `I`.

Tendo isso tudo em mente, espero que já consiga entender melhor como os
componentes dos experimentos estão conectados (chamado de netlist) para formar o
circuito.

No mesmo arquivo lido pelo ngspice, além da netlist definindo as ligações,
existe um bloco delimitado por `.control` e `.endc`, dentro do qual são
inseridas as operações e simulações a serem feitas.

Uma análise AC pode ser feita com o comando `ac`, passando-se o número de
pontos, frequência inicial e frequência final.
O número de pontos pode ser total, por década ou por oitava, dependendo se foi
passado `lin`, `dec` ou `oct`.
Uma análise AC com 1000 pontos por década de 1Hz até 1GHz:
```
ac dec 1000 1 1G
```

O comando `wrdata` pode ser usado para salvar os valores de uma variável em um
arquivo.
Para salvar os valores de tensão do nó no1 após ter feito a análise AC:
```
wrdata nome_arquivo.csv v(no1)
```

Note o `v( )` para se obter o valor de tensão do nó.
Ambas as componentes serão salvas caso o valor seja complexo.
Ao invés de `v` outros podem ser usados: `vr` obtém a parte real, `vi`, a parte
imaginária, `vm` a magnitude, `vp` a fase, e `vdb` a magnitude em dB (20*log10).

A corrente também pode ser obtida usando-se `i( )`, porém apenas a corrente que
circula através de uma fonte de tensão.
Para se obter a corrente que passa pela fonte V1:
```
i(v1)
```

Para obter a corrente que circula através de um componente, insira uma fonte de
tensão de valor 0 em série e então obtenha a corrente dessa fonte.

Também é possível usar variáveis como parâmetros de um componente para que ele
possa ser mudado entre simulações usando o comando `.param` na netlist.
Exemplo:
```
.param valor_pot = 0.5
```

Essa variável pode então ser usada como valor de um componente.
Caso seja usada uma expressão matemática além da variável, é necessário colocar
tudo entre `{}`.
Por exemplo, usando o valor da variável anterior como o ajuste de um
potenciômetro de 50k (o que equivale a dois resistores complementares que sempre
somam 50k):
```
R1pot entrada_pot meio_pot {valor_pot * 50k}
R2pot meio_pot saida_pot {(1 - valor_pot) * 50k}
```

Na seção de controle, pode-se então alterar o valor da variável, para que uma
próxima simulação utilize um novo valor, usando:
```
alterparam valor_pot = 0.8
reset
```

Assim uma próxima simulação utilizará 0.8 como valor do potenciômetro ao invés
de 0.5.
O comando `reset` é importante para que o circuito seja recarregado e alteração
da variável tenha efeito.

Mais uma vez, para mais informações consulte o manual.
Passei por cima de muitos componentes, análises, comandos, etc.
