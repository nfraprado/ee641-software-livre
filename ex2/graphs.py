#!/bin/python
import matplotlib.pyplot as plt
import numpy as np

sits = [
        [np.loadtxt("1_mag.csv"), np.loadtxt("1_phase.csv"), "1"],
        [np.loadtxt("2_mag.csv"), np.loadtxt("2_phase.csv"), "2"],
        [np.loadtxt("3_mag.csv"), np.loadtxt("3_phase.csv"), "3"],
        [np.loadtxt("4_mag.csv"), np.loadtxt("4_phase.csv"), "4"],
        [np.loadtxt("5_mag.csv"), np.loadtxt("5_phase.csv"), "5"],
        [np.loadtxt("6_mag.csv"), np.loadtxt("6_phase.csv"), "6"],
        [np.loadtxt("7_mag.csv"), np.loadtxt("7_phase.csv"), "7"],
]

def save_mag_ph_multiplot(magnitudes, phases, names=["out"], filename="plot.png"):
    subplot = [1, 2]
    subplot_y_label = ["Módulo [dB]", "Fase [$\degree$]"]
    plots = [magnitudes, phases]
    for mag_ph in [0, 1]:
        plt.subplot(2, 1, subplot[mag_ph])
        plt.grid(True, which="major", linestyle='-')
        plt.grid(True, which="minor", linestyle='--')

        plt.ylabel(subplot_y_label[mag_ph])
        # So coloca nome no eixo x da fase, porque e o grafico de baixo
        if mag_ph:
            plt.xlabel("Frequência [Hz]")

        for plot in plots[mag_ph]:
            plt.semilogx(plot[:,0], plot[:,1])
        plt.legend(names)

        plt.savefig(filename, dpi=300)
    plt.clf()

save_mag_ph_multiplot([sits[0][0]], [sits[0][1]], [sits[0][2]], "sit1.png")
# O grafico da situacao 1 e desenhado junto com o grafico de cada situacao para
# servir de referencia
for i in range(2, 8):
    save_mag_ph_multiplot([sits[0][0], sits[i - 1][0]],
                          [sits[0][1], sits[i - 1][1]],
                          [sits[0][2], sits[i - 1][2]],
                          f"sit{i}.png")
