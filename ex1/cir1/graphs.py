#!/bin/python
import matplotlib.pyplot as plt
import numpy as np

set00 = [
    [np.loadtxt("set00_in_mag.csv"), np.loadtxt("set00_out_mag.csv")],
    [np.loadtxt("set00_in_phase.csv"), np.loadtxt("set00_out_phase.csv")],
]
set05 = [
    [np.loadtxt("set05_in_mag.csv"), np.loadtxt("set05_out_mag.csv")],
    [np.loadtxt("set05_in_phase.csv"), np.loadtxt("set05_out_phase.csv")],
]
set10 = [
    [np.loadtxt("set10_in_mag.csv"), np.loadtxt("set10_out_mag.csv")],
    [np.loadtxt("set10_in_phase.csv"), np.loadtxt("set10_out_phase.csv")],
]

def save_mag_ph_multiplot(magnitudes, phases, names=["out"], filename="plot.png"):
    subplot = [1, 2]
    subplot_y_label = ["Módulo [dB]", "Fase [$\degree$]"]
    plots = [magnitudes, phases]
    for mag_ph in [0, 1]:
        plt.subplot(2, 1, subplot[mag_ph])
        plt.grid(True, which="major", linestyle='-')
        plt.grid(True, which="minor", linestyle='--')

        plt.ylabel(subplot_y_label[mag_ph])
        # So coloca nome no eixo x da fase, porque e o grafico de baixo
        if mag_ph:
            plt.xlabel("Frequência [Hz]")

        for plot in plots[mag_ph]:
            plt.semilogx(plot[:,0], plot[:,1])
        plt.legend(names)

        plt.savefig(filename, dpi=300)
    plt.clf()

save_mag_ph_multiplot(set00[0], set00[1], ["in", "out"], "set00.png")
save_mag_ph_multiplot(set05[0], set05[1], ["in", "out"], "set05.png")
save_mag_ph_multiplot(set10[0], set10[1], ["in", "out"], "set10.png")
