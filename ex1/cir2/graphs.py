#!/bin/python
import matplotlib.pyplot as plt
import numpy as np


def save_2plot(plots1, plots2, ylabels, xlabel, names1, names2, filename="plot.png"):
    subplot = [1, 2]
    plots = [plots1, plots2]
    names = [names1, names2]
    for num_plot in [0, 1]:
        plt.subplot(2, 1, subplot[num_plot])
        plt.grid(True, which="major", linestyle='-')
        plt.grid(True, which="minor", linestyle='--')

        plt.ylabel(ylabels[num_plot])
        # So coloca nome no eixo x do grafico inferior
        if num_plot:
            plt.xlabel(xlabel)

        for plot in plots[num_plot]:
            plt.plot(plot[:,0], plot[:,1])
        plt.legend(names[num_plot])

        plt.savefig(filename, dpi=300)
    plt.clf()


# Dados do primeiro grafico, de analise transiente
tran_v = [np.loadtxt("tran_in_e.csv"),
        np.loadtxt("tran_q1_b.csv"),
        np.loadtxt("tran_q1_c.csv"),
        np.loadtxt("tran_q2_c.csv"),
        ]
tran_v_names = ["in_e", "q1_b", "q1_c", "q2_c"]

tran_i = [np.loadtxt("tran_i_diodo.csv")]
tran_i_names = ["Idiodo"]

tran_y_labels = ["Tensão [V]", "Corrente [A]"]
tran_x_label = "Tempo [s]"

# Salva o grafico transiente
save_2plot(tran_v, tran_i, tran_y_labels, tran_x_label, tran_v_names,
           tran_i_names, "tran.png")


# Dados do segundo grafico, de analise sweep DC
dc_v = [np.loadtxt("dc_q1_b.csv"),
      np.loadtxt("dc_q1_c.csv"),
      np.loadtxt("dc_q2_c.csv"),
      ]
dc_v_names = ["q1_b", "q1_c", "q2_c"]

dc_i = [np.loadtxt("dc_i_diodo.csv")]
dc_i_names = ["Idiodo"]

dc_y_labels = ["Tensão [V]", "Corrente [A]"]
dc_x_label = "Tensão de entrada [V]"

# Salva o grafico DC
save_2plot(dc_v, dc_i, dc_y_labels, dc_x_label, dc_v_names, dc_i_names,
           "dc.png")
